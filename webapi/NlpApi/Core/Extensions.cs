﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NlpApi.Core.Extensions
{
  public static class StringExtensions
  {
    public static string Remove(this string s, string text)
    {
      return s.Replace(text, "");

    }

    //public static IEnumerable<TreeNode> Descendants(this TreeNode root)
    //{
    //  var nodes = new Stack<TreeNode>(new[] { root });
    //  while (nodes.Any())
    //  {
    //    TreeNode node = nodes.Pop();
    //    yield return node;
    //    foreach (var n in node.Children) nodes.Push(n);
    //  }
    //}
  }
}