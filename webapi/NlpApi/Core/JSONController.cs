﻿using System;
using System.Text;
using System.Web;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;

namespace NlpApi
{
  public class JSONController<T> : IDisposable, IHTTPController where T : JSONModel, new()
  {
    private Query _query = null;

    private T _record = null;

    /// <summary>
    /// Get the HTTP Context
    /// </summary>
    public HttpContext Context { get; set; }

    public T Record
    {
      get
      {
        return _record;
      }
    }

    /// <summary>
    /// Get the body of the HTTP request
    /// </summary>
    public string Body
    {
      get
      {
        //get post data
        System.IO.Stream path = this.Context.Request.InputStream;
        byte[] bytes = new byte[path.Length];
        path.Position = 0;
        path.Read(bytes, 0, (int)path.Length);
        string data = Encoding.ASCII.GetString(bytes);
        return data;
      }
    }

    public string GetResponse()
    {
      return JsonConvert.SerializeObject(_record);
    }

    public void Init(Query query)
    {
      _query = query;

      if (string.IsNullOrEmpty(Body))
        _record = new T();
      else
        _record = JsonConvert.DeserializeObject<T>(Body);
    }

    public void GetRecord()
    {
      OnRetrieve(this._query);
    }

    public void UpdateRecord()
    { 
     OnCommit(_record);
    }   
    
    public void DeleteRecord()
    {
      OnDelete(_query);
    }

    public void CreateRecord() { 
      OnCreate(_query);
    }

    public virtual void OnRetrieve(Query query) { }
    public virtual void OnCreate(Query query) { }
    public virtual void OnCommit(T record) { }
    public virtual void OnDelete(Query query) { }


    /// <summary>
    /// Remove references
    /// </summary>
    public void Dispose()
    {
    }
  }

}