﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NlpApi
{
  public class Query
  {
    public List<KeyValue> Where;
    public List<string> OrderBy;

    public Query(List<KeyValue> where)
    {
      this.Where = where;
    }

    public Nullable<DateTime> GetNullableDateTime(string key)
    {
      DateTime dtOut;
      bool valid = DateTime.TryParse(this.GetString(key), out dtOut);
      if (!valid)
        return null;
      else
        return dtOut;
    }

    public static Nullable<DateTime> GetNullableDateTimeValue(object value)
    {
      DateTime dtOut;
      bool valid = DateTime.TryParse(Convert.ToString(value), out dtOut);
      if (!valid)
        return null;
      else
        return dtOut;
    }

    public static decimal GetDecimalValue(object value)
    {
      if (!string.IsNullOrEmpty(Convert.ToString(value)))
        return Convert.ToDecimal(value);
      else
        return 0;
    }

    public decimal GetDecimal(string key)
    {
      if (!string.IsNullOrEmpty(Convert.ToString(GetValue(key))))
        return Convert.ToDecimal(GetValue(key));
      else
        return 0;
    }

    public static int GetInt32Value(object value)
    {
      if (!string.IsNullOrEmpty(Convert.ToString(value)))
        return Convert.ToInt32(value);
      else
        return 0;
    }

    public int GetInt32(string key)
    {
      if (!string.IsNullOrEmpty(Convert.ToString(GetValue(key))))
        return Convert.ToInt32(GetValue(key));
      else
        return 0;
    }

    public int GetInt(string key)
    {
      return GetInt32(key);
    }

    public string GetString(string key)
    {
      if (GetValue(key) != null)
      {
        string val = Convert.ToString(GetValue(key));
        if (val == "--- Please Select ---")
          return string.Empty;
        else
          return val;
      }
      else
        return string.Empty;
    }

    public bool GetBool(string key)
    {
      if (GetValue(key) != null)
        return Convert.ToBoolean(GetValue(key));
      else
        return false;
    }

    public object GetValue(string key)
    {
      if (Where == null)
        return null;

      if (Where.Where(k => k.Key.ToLower() == key.ToLower()).FirstOrDefault() != null)
        return Where.Where(k => k.Key.ToLower() == key.ToLower()).FirstOrDefault().Value;
      else
        return null;
    }
  }

  public class KeyValue
  {
    public string Key;
    public string Value;

    public KeyValue() { }

    public KeyValue(string key, string value)
      : this()
    {
      this.Key = key;
      this.Value = value;
    }

    public override string ToString()
    {
      return string.Format("[Key={0}, Value={1}]", this.Key, this.Value);
    }
  }
}