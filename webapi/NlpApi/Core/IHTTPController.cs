﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NlpApi
{
  public interface IHTTPController : IDisposable
  {
    HttpContext Context { get; set; }
    void GetRecord();
    void UpdateRecord();
    void DeleteRecord();
    void CreateRecord();
    void Init(Query query);
    string GetResponse();
  }
}