﻿using System;
using System.Web.UI;
using System.Reflection;
using System.Web;
using System.Web.SessionState;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using NlpApi.Core.Extensions;
using System.Collections.Generic;

namespace NlpApi
{
  public class EntryPointHandler : IHttpHandler, IRequiresSessionState
  {
    public bool IsReusable
    {
      get { return true; }
    }

    public EntryPointHandler()
    {
    }

    public void ProcessRequest(HttpContext context)
    {
      context.Response.Clear();
      HtmlTextWriter hw = new HtmlTextWriter(context.Response.Output);
      string response = string.Empty;

      try
      {
        if (context.Request.FilePath.ToLower().Contains("api/v1/device/controller/"))
        {
          context.Response.ContentType = "application/json";

          // Get the class name
          string[] link = context.Request.FilePath.Split(new char[] { '/' });
          string className = "NlpApi.Controller." + link[link.Length - 1].Replace(".json", "");

          // Resolve the object
          Type[] types = Assembly.GetExecutingAssembly().GetTypes();
          System.Type controllerType = null;
          foreach (Type t in types)
          {
            if (className.ToLower() == t.FullName.ToLower())
            {
              controllerType = t;
              break;
            }
          }

          // Create a new instance
          if (controllerType != null)
          {
            object instance = Activator.CreateInstance(controllerType);
            NlpApi.IHTTPController controller = instance as NlpApi.IHTTPController;
            controller.Context = context;

            // Get querystrings
            List<KeyValue> parameters = new List<KeyValue>();
            foreach (var key in context.Request.QueryString.AllKeys)
            {
              parameters.Add(new KeyValue(key, context.Request.QueryString[key]));
            }
            Query query = new Query(parameters);
            controller.Init(query);

            switch (context.Request.HttpMethod)
            {
              case "GET":
                controller.GetRecord();
                break;
              case "POST":
                controller.UpdateRecord();
                break;
              case "PUT":
                controller.UpdateRecord();
                break;
              case "DELETE":
                controller.DeleteRecord();
                break;
            }

            response = controller.GetResponse();
            hw.Write(response);
          }

        }
        else
        {
          // Report error
          throw new Exception("Invalid API Method");
        }
      }
      catch (Exception ex)
      {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter jsonWriter = new JsonTextWriter(sw))
        {
          jsonWriter.WriteStartObject();
          jsonWriter.WritePropertyName("IsValid");
          jsonWriter.WriteValue(false);
          jsonWriter.WritePropertyName("BrokenRules");
          jsonWriter.WriteStartArray();
          jsonWriter.WriteRaw("{\"Description\": \"" + JsonClean(ex.Message) + "\", \"ValueKey\": \"\"}");
          jsonWriter.WriteEnd();
          jsonWriter.WriteEndObject();
        }

        hw.Write(sb.ToString());
      }
      finally
      {
        hw.Close();
        context.Response.End();
      }
    }

    private string JsonClean(string text)
    {
      return text.Remove("\n").Remove("\t").Remove("\r");
    }

  }
}
