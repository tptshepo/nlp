﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NlpApi.Utils
{
  public static class Database
  {
    public static string Staging = "DB:Staging";
    public static string SuperBoss = "DB:Main";
    public static string NedDash = "DB:NedDash";
    public static string Forms = "DB:Forms";

    public static List<List<string>> GetDatabaseObjects(string connectionString)
    {
      List<List<string>> library = new List<List<string>>();

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ConnectionString))
      {
        conn.Open();
        using (SqlCommand cm = conn.CreateCommand())
        {
          cm.CommandText = @"SELECT  
	T.NAME AS [TABLE NAME], 
	C.NAME AS [COLUMN NAME]
FROM SYS.OBJECTS AS T
JOIN SYS.COLUMNS AS C
ON T.OBJECT_ID=C.OBJECT_ID
JOIN SYS.TYPES AS P
ON C.SYSTEM_TYPE_ID=P.SYSTEM_TYPE_ID
WHERE T.TYPE_DESC='USER_TABLE'
ORDER BY [TABLE NAME]";

          cm.CommandType = CommandType.Text;
          SqlDataReader reader = cm.ExecuteReader(CommandBehavior.CloseConnection);
          while (reader.Read())
          {
            List<string> items = new List<string>();
            items.Add(reader.GetString(0));
            items.Add(reader.GetString(1));

            library.Add(items);
          }
        }
      }

      return library;
    }

    public static List<List<string>> GetDatabaseData(string connectionString, string sql)
    {
      List<List<string>> db = new List<List<string>>();

      using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connectionString].ConnectionString))
      {
        conn.Open();
        using (SqlCommand cm = conn.CreateCommand())
        {
          cm.CommandText = sql;
          cm.CommandType = CommandType.Text;
          SqlDataReader reader = cm.ExecuteReader(CommandBehavior.CloseConnection);

          int count = 0;
          while (reader.Read())
          {
            int colCount = reader.FieldCount;

            if (count == 0)
            {
              count++;

              List<string> header = new List<string>();
              for (int i = 0; i < colCount; i++)
              {
                header.Add(reader.GetName(i));
              }
              db.Add(header);
            }

            List<string> row = new List<string>();
            for (int i = 0; i < colCount; i++)
            {
              row.Add(Convert.ToString(reader.GetValue(i)));
            }

            db.Add(row);
          }
        }
      }

      return db;
    }

  }
}