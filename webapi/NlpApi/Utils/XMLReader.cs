﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace NlpApi.Utils
{
  public class XMLReader : IDisposable
  {
    private List<XMLTag> _xmlElements = null;

    public XMLReader()
    {
      _xmlElements = new List<XMLTag>();
    }

    public void ParseXML(string xmlData)
    {
      XMLTag tags = this.Load(xmlData);
      _xmlElements = FlattenTags(tags);
    }

    public void ParseXMLFile(string xmlFile)
    {
      XMLTag tags = this.Load(xmlFile, true);
      _xmlElements = FlattenTags(tags);
    }

    public XMLTag GetTag(string localName)
    {
      return (from el in _xmlElements where (el.Name.ToLower() == localName.ToLower()) select el).FirstOrDefault();
    }

    public IEnumerable<XMLTag> GetTags(string localName)
    {
      return (from el in _xmlElements where (el.Name.ToLower() == localName.ToLower()) select el);
    }

    public List<XMLTag> GetElements
    {
      get
      {
        return _xmlElements;
      }
    }

    public string GetChildElement(XMLTag parent, string localName)
    {
      XMLTag child = (from el in parent.ChildTags where (el.Name.ToLower() == localName.ToLower()) select el).FirstOrDefault();
      if (child != null)
      {
        return child.InnerText;
      }
      return string.Empty;
    }

    public string GetElement(string localName)
    {
      XMLTag tag = GetTag(localName);
      if (tag != null)
      {
        return tag.InnerText;
      }

      return string.Empty;
    }

    public string GetAttribute(XMLTag tag, string localName)
    {
      var ret = (from t in tag.Attributes where (t.Name.ToLower() == localName.ToLower()) select t).FirstOrDefault();
      if (ret != null)
        return ret.Value;

      return string.Empty;
    }

    private XMLTag Load(string xmlString)
    {
      XMLTag xmlData = new XMLTag();

      using (XmlTextReader reader = new XmlTextReader(StringToStream(xmlString)))
      {
        XMLTag currentTag = new XMLTag(); //References the current tag you are busy with
        XMLTag tag = new XMLTag();

        int tagID = 0;
        int TagIndex = -1;

        reader.Namespaces = false;

        while (reader.Read())
        {
          tagID++;

          #region . OPEN TAG .
          if (reader.NodeType == XmlNodeType.Element) //Open Tag
          {
            TagIndex++;
            if (TagIndex == 0) //fist tag
            {
              tag = new XMLTag();

              tag.Name = reader.Name.ToLower();
              tag.Id = tagID;
              if (reader.HasAttributes)
              {
                while (reader.MoveToNextAttribute())
                {
                  tag.Attributes.Add(new XMLAttribute(reader.Name, reader.Value));
                }
              }

              xmlData = (XMLTag)tag;
              currentTag = (XMLTag)tag;
            }
            else
            {
              XMLTag childTag = new XMLTag();
              childTag.ParentTag = (XMLTag)currentTag;
              childTag.Name = reader.Name.ToLower();
              childTag.Id = tagID;
              if (reader.HasAttributes)
              {
                while (reader.MoveToNextAttribute())
                {
                  childTag.Attributes.Add(new XMLAttribute(reader.Name, reader.Value));
                }
              }

              currentTag.ChildTags.Add(childTag);
              currentTag = (XMLTag)childTag;
            }

            //checks if this is an empty tag <empty/> not <empty></empty>
            reader.IsStartElement();
            if (reader.IsEmptyElement)
            {
              currentTag.IsTagEmpty = true;
              currentTag = (XMLTag)currentTag.ParentTag; //move to previous node
              continue;
            }
          }
          #endregion

          #region . INNER TEXT .
          if (reader.NodeType == XmlNodeType.Text) //Inner Text
          {
            currentTag.InnerText = reader.Value;
          }
          if (reader.NodeType == XmlNodeType.CDATA) //CDATA
          {
            currentTag.InnerText = reader.Value;
          }
          #endregion

          #region . CLOSE TAG  .
          if (reader.NodeType == XmlNodeType.EndElement) //Close Tag
          {
            currentTag = (XMLTag)currentTag.ParentTag;
          }
          #endregion
        }
      }

      return xmlData;
    }
    private XMLTag Load(string xmlfile, bool isfile)
    {
      XMLTag xmlData = new XMLTag();

      using (XmlTextReader reader = new XmlTextReader(xmlfile))
      {
        XMLTag currentTag = new XMLTag(); //References the current tag you are busy with
        XMLTag tag = new XMLTag();

        int tagID = 0;
        int TagIndex = -1;

        reader.Namespaces = false;

        while (reader.Read())
        {
          tagID++;

          #region . OPEN TAG .
          if (reader.NodeType == XmlNodeType.Element) //Open Tag
          {
            TagIndex++;
            if (TagIndex == 0) //fist tag
            {
              tag = new XMLTag();

              tag.Name = reader.Name.ToLower();
              tag.Id = tagID;
              if (reader.HasAttributes)
              {
                while (reader.MoveToNextAttribute())
                {
                  tag.Attributes.Add(new XMLAttribute(reader.Name, reader.Value));
                }
              }

              xmlData = (XMLTag)tag;
              currentTag = (XMLTag)tag;
            }
            else
            {
              XMLTag childTag = new XMLTag();
              childTag.ParentTag = (XMLTag)currentTag;
              childTag.Name = reader.Name.ToLower();
              childTag.Id = tagID;
              if (reader.HasAttributes)
              {
                while (reader.MoveToNextAttribute())
                {
                  childTag.Attributes.Add(new XMLAttribute(reader.Name, reader.Value));
                }
              }

              currentTag.ChildTags.Add(childTag);
              currentTag = (XMLTag)childTag;
            }

            //checks if this is an empty tag <empty/> not <empty></empty>
            reader.IsStartElement();
            if (reader.IsEmptyElement)
            {
              currentTag.IsTagEmpty = true;
              currentTag = (XMLTag)currentTag.ParentTag; //move to previous node
              continue;
            }
          }
          #endregion

          #region . INNER TEXT .
          if (reader.NodeType == XmlNodeType.Text) //Inner Text
          {
            currentTag.InnerText = reader.Value;
          }
          if (reader.NodeType == XmlNodeType.CDATA) //CDATA
          {
            currentTag.InnerText = reader.Value;
          }
          #endregion

          #region . CLOSE TAG  .
          if (reader.NodeType == XmlNodeType.EndElement) //Close Tag
          {
            currentTag = (XMLTag)currentTag.ParentTag;
          }
          #endregion
        }
      }

      return xmlData;
    }

    private List<XMLTag> FlattenTags(XMLTag input)
    {
      List<XMLTag> elements = new List<XMLTag>();
      elements.Add(input);
      ReadTag(input, ref elements);
      return elements;
    }

    private void ReadTag(XMLTag tag, ref List<XMLTag> elements)
    {
      foreach (XMLTag childTag in tag.ChildTags)
      {
        //read current tag
        elements.Add(childTag);

        // move to next tag
        ReadTag(childTag, ref elements);
      }
    }

    public static Stream StringToStream(string text)
    {
      System.Text.ASCIIEncoding myEncoder = new System.Text.ASCIIEncoding();
      byte[] bytes = myEncoder.GetBytes(text);
      return (Stream)new MemoryStream(bytes);
    }

    #region IDisposable Members

    public void Dispose()
    {
      _xmlElements = null;
    }

    #endregion
  }

  #region XML Objects
  /// <summary>
  /// Represents an attribute of a tag
  /// </summary>
  public class XMLAttribute
  {
    private string _name = string.Empty;
    private string _value = string.Empty;

    /// <summary>
    /// Constructor
    /// </summary>
    public XMLAttribute()
    {

    }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="Name">Name of the attriute</param>
    /// <param name="Value">Value of the attribute</param>
    public XMLAttribute(string Name, string Value)
    {
      _name = Name;
      _value = Value;
    }


    /// <summary>
    /// Name of the attribute
    /// </summary>
    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    /// <summary>
    /// Value of the attribute
    /// </summary>
    public string Value
    {
      get { return _value; }
      set { _value = value; }
    }

    public override string ToString()
    {
      return _name;
    }
  }
  public class XMLTag
  {
    private int _id;
    private string _name = string.Empty;
    private List<XMLAttribute> _attributes = new List<XMLAttribute>();
    private List<XMLTag> _childTags = new List<XMLTag>();
    private string _innerText = string.Empty;
    private bool _isTagEmpty = false;
    private XMLTag _ParentTag;

    /// <summary>
    /// Gets or sets a value indicating the unique id of the tag
    /// </summary>
    public int Id
    {
      get { return _id; }
      set { _id = value; }
    }

    /// <summary>
    /// Gets or sets the name of the tag
    /// </summary>
    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    /// <summary>
    /// Gets or sets a list of attributes
    /// </summary>
    public List<XMLAttribute> Attributes
    {
      get { return _attributes; }
      set { _attributes = value; }
    }

    /// <summary>
    /// Gets or sets a list of child tags
    /// </summary>
    public List<XMLTag> ChildTags
    {
      get { return _childTags; }
      set { _childTags = value; }
    }

    /// <summary>
    /// Gets or sets the parent tag
    /// </summary>
    public XMLTag ParentTag
    {
      get { return _ParentTag; }
      set { _ParentTag = value; }
    }

    /// <summary>
    /// Gets or sets a value indicating if the tag is an emapty tag
    /// </summary>
    public bool IsTagEmpty
    {
      get { return _isTagEmpty; }
      set { _isTagEmpty = value; }
    }

    /// <summary>
    /// Gets or sets the tag text
    /// </summary>
    public string InnerText
    {
      get { return _innerText; }
      set { _innerText = value.Trim(); }
    }

    public string GetAttributeValue(string name)
    {
      foreach (XMLAttribute att in this.Attributes)
      {
        if (att.Name.ToLower() == name.ToLower())
        {
          return att.Value;
        }
      }
      return string.Empty;
    }

    public object GetAttributeValue(string name, Type type, string defaultValue)
    {
      foreach (XMLAttribute att in this.Attributes)
      {
        if (att.Name.ToLower() == name.ToLower())
        {
          return Convert.ChangeType(att.Value, type);
        }
      }
      return Convert.ChangeType(defaultValue, type);
    }

    public override string ToString()
    {
      return _name;
    }

    public XMLTag()
    {
    }

    public XMLTag(string tagName)
    {
      _name = tagName;
    }

  }
  #endregion
}
