﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NlpApi.Utils.Extensions
{
  public static class Extensions
  {
    public static IEnumerable<XMLTag> Descendants(this XMLTag root)
    {
      var nodes = new Stack<XMLTag>(new[] { root });
      while (nodes.Any())
      {
        XMLTag node = nodes.Pop();
        yield return node;
        if (node.ChildTags != null)
        {
          foreach (var n in node.ChildTags)
            nodes.Push(n);
        }
      }
    }

  }
}