﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NlpApi.Utils;

namespace NlpApi.Controller
{
  public class NlpQuery : JSONController<NlpQueryModel>
  {
    public override void OnCommit(NlpQueryModel record)
    {
      if (string.IsNullOrEmpty(record.Sql))
      {
        this.Record.IsValid = false;
        return;
      }

      this.Record.Data = Database.GetDatabaseData(Database.SuperBoss, record.Sql);
      this.Record.Sql = "";
    }
  }


  public class NlpQueryModel : JSONModel
  {
    public string Sql = string.Empty;
    public List<List<string>> Data = new List<List<string>>();
  }


}