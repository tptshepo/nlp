﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NlpApi.Utils;

namespace NlpApi.Controller
{
  public class Dictionary : JSONController<DictionaryModel>
  {
    public override void OnRetrieve(Query query)
    {
      this.Record.Library = Database.GetDatabaseObjects(Database.SuperBoss);
    }
  }


  public class DictionaryModel : JSONModel
  {
    public List<List<string>> Library = new List<List<string>>();
  }


}