var Q = require('q');
var db = require('./dictionary')
var requiredb = require('./requiredb');
var webapi = require('./webapi');

var rules = [

    {
        context: ['all', 'accounts/opened', 'open/opened/accounts'],
        select: ['*'],
        where: []
    },

    {
        context: ['what', 'status', 'account', 'number'],
        select: ['*'],
        where: [{
            field: 'account.accountNo',
            type: 'string'
        }]
    },

    {
        context: ['many', 'accounts', 'opened', 'between'],
        select: ['count(*)'],
        where: [{
            field: 'vw_workflowmonitor.EntryDate',
            sign: '>=',
            type: 'date'
        }, {
            field: 'vw_workflowmonitor.EntryDate',
            sign: '<=',
            type: 'date'
        }]
    },

    {
        context: ['accounts', 'opened', 'between'],
        select: ['*'],
        where: [{
            field: 'vw_workflowmonitor.EntryDate',
            sign: '>=',
            type: 'date'
        }, {
            field: 'vw_workflowmonitor.EntryDate',
            sign: '<=',
            type: 'date'
        }]
    },

    {
        context: ['many/much', 'accounts', 'opened', 'on'],
        select: ['count(*)'],
        where: [{
            field: 'Convert(varchar, vw_workflowmonitor.EntryDate, 111)',
            type: 'date'
        }]
    },

    {
        context: ['accounts', 'opened', 'on'],
        select: ['*'],
        where: [{
            field: 'Convert(varchar, vw_workflowmonitor.EntryDate, 111)',
            type: 'date'
        }]
    }

];


exports.parse = function(input) {

    return Q.fcall(function() {

        var rule;
        var found_rule = true;

        // loop rule items
        for (var i = 0; i < rules.length; i++) {

            rule = rules[i];
            found_rule = true;

            // loop word item
            for (var w = 0; w < rule.context.length; w++) {

                var searchWord = rule.context[w].split('/');

                /* 
                	find a matching context to the sentence 
                */

                var match_found = false;
                for (var s = 0; s < searchWord.length; s++) {
                    if (input.sentence.indexOf(searchWord[s]) > -1) {
                        // found word
                        match_found = true;
                        break;
                    }
                }

                if (!match_found) {
                    // word not found
                    //console.log("NOT_FOUND: " + searchWord)
                    found_rule = false;
                    break;
                }

            }

            if (found_rule) {
                /*
					matching rule found, exit the rule loop
            	*/
                break;
            }

        }

        if (!found_rule) {
            throw new Error("I don't understand. Please re-phrase the question.");
        }

        return rule;
    })

    .then(function(rule) {

        /*
			Resolve the where clauses values
		*/

        var error = "Your question appears to be incomplete, you might be missing a filter.";

        for (var i = 0; i < rule.where.length; i++) {
            var where = rule.where[i];
            where.values = [];

            /* check if the numbers we received in the input matches the
                where clause of this rule object. If it doesn't, then 
                we will assume they are trying to query on multiple number.
            */

            var no_count = input.tags.numbers.length;
            var use_all_numbers = false;
            if (rule.where.length < no_count) {
                use_all_numbers = true;
            }


            if (where.type === 'int') {

                /* get the value */
                if (use_all_numbers) {
                    // use all the numbers provided
                    for (var n = 0; n < input.tags.numbers.length; n++) {
                        where.values.push(input.tags.numbers[n]);
                    }
                } else {
                    var v = input.tags.numbers[i];
                    if (v === undefined) {
                        throw new Error(error);
                    }
                    where.values.push(v);
                }

            } else if (where.type === 'string') {

                /* get the value */
                if (use_all_numbers) {
                    // use all the numbers provided
                    for (var n = 0; n < input.tags.numbers.length; n++) {
                        where.values.push("'" + input.tags.numbers[n] + "'");
                    }
                } else {
                    var v = input.tags.numbers[i];
                    if (v === undefined) {
                        throw new Error(error);
                    }
                    where.values.push("'" + v + "'");
                }


            } else if (where.type === 'date') {

                /* get the value */
                var v = input.tags.dates[i];
                if (v === undefined) {
                    throw new Error(error);
                }
                where.values.push("'" + v + "'");

            } else {
                throw new Error('filter type not defined');
            }


        }


        return rule;

    })


    .then(function(rule) {

        /* 
            Resolve select fields
         */

        //debugger;
        var find_list = [].concat(input.tags.singularnouns)
            .concat(input.tags.pluralnouns)
            .concat(input.tags.pastverb)
            .concat(input.tags.presentverb);

        var dblist = db.library();
        var fields = [];

        for (var f = 0; f < find_list.length; f++) {

            for (var i = 0; i < dblist.length; i++) {

                if (dblist[i][1] === find_list[f]) {
                    console.log(dblist[i])
                    fields.push[dblist[i]];
                }

            }
        }

        if (input.tags.propernoun.indexOf("show") > -1) {
            if (fields.length === 0) {
                throw new Error('I do not understand the field names you want to show');
            }
        }


        return rule;
    })

    .then(function(rule) {

        /*
			Build the query string
    	*/

        var sql = requiredb.sql(input.sentence);

        // resolve select
        sql = sql.replace('{select}', rule.select.join(','));

        // resolve where
        var where_str = '';

        for (var i = 0; i < rule.where.length; i++) {
            var where = rule.where[i];

            if (where.sign !== undefined && where.sign !== '') {

                where_str += ' AND ' + where.field + ' ' + where.sign + ' ' + where.values.join(',')

            } else {

                where_str += ' AND ' + where.field + ' IN (' + where.values.join(',') + ')'
            }
        }

        sql = sql.replace('{where}', where_str);

        var ret = {'sql': sql}
        return ret;

    });

}