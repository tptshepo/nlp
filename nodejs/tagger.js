var utils = require('./utils');



var clean_response = function(text) {
    var split = text.split('\n');
    if (split.length === 1) {
        split = text.split(' ');
    }
    var words = [];
    for (var i = 0; i < split.length; i++) {
        if (i === 0) {
            continue;
        }
        split[i] = split[i].replace(')', '').trim();
        split[i] = split[i].replace('(ORGANIZATION ', '').trim();
        split[i] = split[i].replace('(PERSON ', '').trim();

        var word = [];
        if (split[i].indexOf('//CD') > -1) {
            word = ['/', 'CD'];
        } else {
            word = split[i].split('/');
        }
        words.push(word);
    }
    //console.log(words);
    return words;
};

exports.rephrase = function(text) {

    var reg = /(\d{2})T(\d{2})/g;
    var matches = [],
        found;
    while (found = reg.exec(text)) {
        matches.push(found[0]);
    }

    for (var i = 0; i < matches.length; i++) {
        text = text.replace(matches[i], matches[i].replace('T', '*'))
    }

    return text;
};

exports.parse = function(text) {

    var words = clean_response(text)
    var sentence = '';

    var tags = {
        whpronoun: [],
        whadverbs: [],
        adverb: [],
        adjectives: [],
        singularnouns: [],
        pluralnouns: [],
        pastverb: [],
        presentverb: [],
        propernoun: [],
        conjunction: [],
        numbers: [],
        personalpronoun: [],
        dates: [],
        determiner: []
    };

    var cd_list = '';

    for (var i = 0; i < words.length; i++) {
        var w = words[i];

        if (w[1] === "CD" || w[1] === ":" || w[1] === '-NONE-') {
            if (w[1] === '-NONE-') {
                cd_list += 'T';
            } else {
                cd_list += w[0].toLowerCase();
            }
            continue;
        } else if (cd_list !== '') {
            sentence += cd_list + ' ';

            if (utils.isDateValid(cd_list)) {
                tags.dates.push(cd_list);
            } else {
                tags.numbers.push(cd_list);
            }
            cd_list = '';

        }

        sentence += w[0].toLowerCase() + ' ';

        if (w[1] === "WRB") {
            tags.whadverbs.push(w[0].toLowerCase());
        } else if (w[1] === "JJ") {
            tags.adjectives.push(w[0].toLowerCase());
        } else if (w[1] === "NN") {
            tags.singularnouns.push(w[0].toLowerCase());
        } else if (w[1] === "NNS") {
            tags.pluralnouns.push(w[0].toLowerCase());
        } else if (w[1] === "VBN") {
            tags.pastverb.push(w[0].toLowerCase());
        } else if (w[1] === "VBD") {
            tags.pastverb.push(w[0].toLowerCase());
        } else if (w[1] === "NNP") {
            tags.propernoun.push(w[0].toLowerCase());
        } else if (w[1] === "IN") {
            tags.conjunction.push(w[0].toLowerCase());
        } else if (w[1] === "RB") {
            tags.adverb.push(w[0].toLowerCase());
        } else if (w[1] === "WP") {
            tags.whpronoun.push(w[0].toLowerCase());
        } else if (w[1] === "VBZ") {
            tags.presentverb.push(w[0].toLowerCase());
        } else if (w[1] === "PRP") {
            tags.personalpronoun.push(w[0].toLowerCase());
        } else if (w[1] === "DT") {
            tags.determiner.push(w[0].toLowerCase());
        }

    }

    return {
        'tags': tags,
        'sentence': sentence
    };
}