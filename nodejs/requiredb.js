var config = {

    joins: {

        /*superboss*/
        'account': 'superboss..account with(nolock)',
        'accounttype': 'inner join superboss..accounttype with(nolock) on account.accounttypeid = accounttype.accounttypeid',
        'accountchannel': 'inner join superboss..accountchannel with(nolock) on account.accountchannelid = accountchannel.accountchannelid',
        'customer': 'inner join superboss..customer with(nolock) on customer.customerid = account.customerid',
        'accountprocessing': 'left join superboss..accountprocessing with(nolock) on account.accountid = accountprocessing.accountid',

        'cellcontact': 'left join superboss..customercontact cellcontact with(nolock) on cellcontact.customercontactid = (select top 1 customercontactid from superboss..customercontact where customer.customerid = customercontact.customerid and contacttypeid = 1 and number is not null)',
        'workcontact': 'left join superboss..customercontact workcontact with(nolock) on workcontact.customercontactid = (select top 1 customercontactid from superboss..customercontact where customer.customerid = customercontact.customerid and contacttypeid = 4 and number is not null)',
        'homecontact': 'left join superboss..customercontact homecontact with(nolock) on homecontact.customercontactid = (select top 1 customercontactid from superboss..customercontact where customer.customerid = customercontact.customerid and contacttypeid = 3 and number is not null)',
        'emailcontact': 'left join superboss..customercontact emailcontact with(nolock) on emailcontact.customercontactid = (select top 1 customercontactid from superboss..customercontact where customer.customerid = customercontact.customerid and contacttypeid = 5 and number is not null)',
        'postaladdress': 'left join superboss..customeraddress postaladdress with(nolock) on postaladdress.customeraddressid = (select top 1 customeraddressid from superboss..customeraddress where customer.customerid = customeraddress.customerid and addresstypeid = 1)',
        'residentialaddress': 'left join superboss..customeraddress residentialaddress with(nolock) on residentialaddress.customeraddressid = (select top 1 customeraddressid from superboss..customeraddress where customer.customerid = customeraddress.customerid and addresstypeid = 2)',

        /*yflow*/
        'workflowaudit': 'inner join yflow..workflowaudit with(nolock) on workflowaudit.workflowentityid = account.workflowentityid',
        'workflowlocation': 'inner join yflow..workflowlocation with(nolock) on workflowaudit.workflowlocationid = workflowlocation.workflowlocationid',
        'vw_workflowmonitor': 'inner join yflow.dbo.vw_workflowmonitor	with(nolock) on vw_workflowmonitor.workflowentityid = account.workflowentityid'

    },

    deps: {

        'accounttype': ['account'],
        'accountchannel': ['account'],
        'customer': ['account'],
        'accountprocessing': ['account'],

        'cellcontact': ['customer'],
        'workcontact': ['customer'],
        'homecontact': ['customer'],
        'emailcontact': ['customer'],
        'postaladdress': ['customer'],
        'residentialaddress': ['customer'],

        'workflowaudit': ['account'],
        'workflowlocation': ['workflowaudit'],
        'vw_workflowmonitor': ['account']
    },

    where: {

        'workflowaudit': "workflowaudit.direction = 'IN'",
        'workflowlocation': "workflowlocation.name = 'Initialized'"

    },

    phrase: {

        'account': ['customer', 'workflowaudit', 'workflowlocation', 'vw_workflowmonitor'],
        'accounts': ['customer', 'workflowaudit', 'workflowlocation', 'vw_workflowmonitor'],
        'opened': ['customer', 'workflowaudit', 'workflowlocation', 'vw_workflowmonitor'],
        'status': ['customer', 'workflowaudit', 'workflowlocation', 'vw_workflowmonitor']
    }

};

var getEntities = function(input) {
    var entities = [];
    var words = input.split(' ');
    for (var w = 0; w < words.length; w++) {

        var token = config.phrase[words[w]];
        if (token !== undefined) {
            for (var t = 0; t < token.length; t++) {
                if (entities.indexOf(token[t]) === -1) {
                    entities.push(token[t]);
                }
            }
        }
    }
    return entities;
};

exports.sql = function(input) {
    var sql = '';
    var entities = getEntities(input);

    sql += 'SELECT TOP 50\n';
    sql += '{select}\n';
    sql += 'FROM\n';

    var resolved_list = [];

    var recursive_search = function(entity) {

        // log entity as processed
        if (resolved_list.indexOf(entity) === -1) {
            resolved_list.push(entity);
        } else {
            return;
        }

        // get a list of dependecies for the entity
        var deps_list = config.deps[entity];
        if (deps_list !== undefined) {
            for (var i = 0; i < deps_list.length; i++) {
                var dep_entity = deps_list[i];
                recursive_search(dep_entity);
            }
        }

        // inject join statement
        var s = config.joins[entity];
        if (s !== undefined) {
            sql += s + '\n';
        }
    }


    for (var i = 0; i < entities.length; i++) {
        var entity = entities[i];
        // processing dependecies
        recursive_search(entity);
    }

    sql += "WHERE\n";

    //inject standard where clauses
    var index = 0;
    for (var i = 0; i < resolved_list.length; i++) {
        var wh = config.where[resolved_list[i]];
        if (wh !== undefined) {
            if (index > 0) {
                sql += 'AND ';
            }
            sql += wh + "\n";
            index++;
        }
    }

    sql += "{where}\n";

    return sql;
};