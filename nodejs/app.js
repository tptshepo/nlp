var unirest = require('unirest');
var tagger = require('./tagger');
var query = require('./query');
var Q = require('q');
var webapi = require('./webapi');


var question = "Show firstname and lastname for accounts that been opened between 2014/10/01 and 2014/10/31";
//var question = "Show gender for accounts that been opened between 2014/10/01 and 2014/10/31";
//var question = "accounts opened between 2014/10/01 and 2014/10/31 please show the firstname and lastname";
//var question = "show accounts opened between 2014/10/01 and 2014/10/31";
//var question = "accounts opened between 2014/10/01 and 2014/10/31";
//var question = "What accounts have been opened between 2014/10/01 and 2014/10/02";
//var question = "what is the status of account number 1234567890";
//var question = "Show all accounts that are opened.";
//var question = "How many accounts were opened on the 2014/10/18";








Q.fcall(function() {
    return tagger.rephrase(question);
}).then(function(sentence) {
    unirest.post("https://japerk-text-processing.p.mashape.com/tag/")
        .header("X-Mashape-Key", "6xzxFKjyMimshHdqMa7xTyqqgPhEp1EQHIJjsnprVbxOMkrIcA")
        .field("text", sentence + '.')
        .end(function(result) {
            //console.log(result.body.text);
            var tagOutput = tagger.parse(result.body.text);
            //console.log(tagOutput);
            query.parse(tagOutput)
            .then(webapi.nlpQuery)
            .then(function(res) {
                console.log(res);
            })
            .fail(function(error) {
                console.log(error);
            }).done();
        });
});