var S  = require('string');

exports.today = function() {
    var s;
    var zeroPad = function(num, places) {
        var zero = places - num.toString().length + 1;
        return Array(+(zero > 0 && zero)).join("0") + num;
    };
    var d = (new Date()).getDate();
    var m = (new Date()).getMonth() + 1;
    var y = (new Date()).getFullYear();

    var h = (new Date()).getHours();
    var mm = (new Date()).getMinutes();
    var s = (new Date()).getSeconds();

    s = zeroPad(y, 4) + '-' + zeroPad(m, 2) + '-' + zeroPad(d, 2) + 'T' + zeroPad(h, 2) + '-' + zeroPad(mm, 2) + '-' + zeroPad(s, 2);

    return s;
};

exports.isDateValid = function(str) {
    var dateString = str;

    if (dateString.indexOf('T') > -1){
        dateString = dateString.split('T')[0];
    }

    dateString = S(dateString).replaceAll('/', '').s;
    if (dateString.length == 8) {
        var y = parseInt(dateString.substr(0, 4), 10);
        var m = parseInt(dateString.substr(4, 2), 10);
        var d = parseInt(dateString.substr(6, 2), 10);

        if (!(y >= 1900 && y <= 3000)) {
            return false;
            //invalid year /*if this system is still being used by 3000, then the future sucks!!*/
        }
        if (!(m >= 1 && m <= 12)) {
            return false;
            //invalid month
        }
        if (!(d >= 1 && d <= 31)) {
            return false;
            //invalid day
        }

        var dateSplit = [y, m, d];
        var newDate = dateSplit.join('/');

        return !isNaN(new Date(newDate));
    } else {
        return false;
    }
};