var http = require('http');
var S = require('string');
var Q = require('q');

var server = 'cerberus-pc';
var port = 80;
var virtualDirectory = '/NlpApi';
var controllerPath = '/api/v1/device/controller/';

exports.nlpQuery = function(sql) {
    var deferred = Q.defer();

	var str = JSON.stringify(sql);
    
    var options = {
        host: server,
        port: port,
        path: virtualDirectory + controllerPath + 'NlpQuery.json',
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Length': str.length
        }
    };
    var responseData = '';
    var reqPOST = http.request(options, function(res) {
        res.setEncoding('utf8');

        res.on('data', function(chunk) {
            responseData += chunk.toString();
        });

        res.on('end', function(e) {
        	debugger;
            deferred.resolve(JSON.parse(responseData));
        });
    });
    // post the data
debugger;
    reqPOST.write(str);
    reqPOST.end();
    reqPOST.on('error', function(e) {
        deferred.reject(new Error(e));
    });

    return deferred.promise;
};